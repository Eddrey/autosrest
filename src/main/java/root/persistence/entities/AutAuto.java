
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "aut_autos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutAuto.findAll", query = "SELECT a FROM AutAuto a"),
    @NamedQuery(name = "AutAuto.findByAutNumeroSerie", query = "SELECT a FROM AutAuto a WHERE a.autNumeroSerie = :autNumeroSerie"),
    @NamedQuery(name = "AutAuto.findByAutMarca", query = "SELECT a FROM AutAuto a WHERE a.autMarca = :autMarca"),
    @NamedQuery(name = "AutAuto.findByAutModelo", query = "SELECT a FROM AutAuto a WHERE a.autModelo = :autModelo"),
    @NamedQuery(name = "AutAuto.findByAutProcedencia", query = "SELECT a FROM AutAuto a WHERE a.autProcedencia = :autProcedencia"),
    @NamedQuery(name = "AutAuto.findByAutFechaFabricacion", query = "SELECT a FROM AutAuto a WHERE a.autFechaFabricacion = :autFechaFabricacion")})
public class AutAuto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "aut_numero_serie")
    private String autNumeroSerie;
    @Size(max = 2147483647)
    @Column(name = "aut_marca")
    private String autMarca;
    @Size(max = 2147483647)
    @Column(name = "aut_modelo")
    private String autModelo;
    @Size(max = 2147483647)
    @Column(name = "aut_procedencia")
    private String autProcedencia;
    @Size(max = 2147483647)
    @Column(name = "aut_fecha_fabricacion")
    private String autFechaFabricacion;

    public AutAuto() {
    }

    public AutAuto(String autNumeroSerie) {
        this.autNumeroSerie = autNumeroSerie;
    }

    public String getAutNumeroSerie() {
        return autNumeroSerie;
    }

    public void setAutNumeroSerie(String autNumeroSerie) {
        this.autNumeroSerie = autNumeroSerie;
    }

    public String getAutMarca() {
        return autMarca;
    }

    public void setAutMarca(String autMarca) {
        this.autMarca = autMarca;
    }

    public String getAutModelo() {
        return autModelo;
    }

    public void setAutModelo(String autModelo) {
        this.autModelo = autModelo;
    }

    public String getAutProcedencia() {
        return autProcedencia;
    }

    public void setAutProcedencia(String autProcedencia) {
        this.autProcedencia = autProcedencia;
    }

    public String getAutFechaFabricacion() {
        return autFechaFabricacion;
    }

    public void setAutFechaFabricacion(String autFechaFabricacion) {
        this.autFechaFabricacion = autFechaFabricacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autNumeroSerie != null ? autNumeroSerie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutAuto)) {
            return false;
        }
        AutAuto other = (AutAuto) object;
        if ((this.autNumeroSerie == null && other.autNumeroSerie != null) || (this.autNumeroSerie != null && !this.autNumeroSerie.equals(other.autNumeroSerie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.AutAuto[ autNumeroSerie=" + autNumeroSerie + " ]";
    }
    
}
