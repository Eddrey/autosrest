
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.dao.AutAutoDAO;
import root.persistence.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.AutAuto;

@Path("/autos")
public class AutosRest {
    /**
     * Llamar DAO
     */
    
    AutAutoDAO dao = new AutAutoDAO();
    
    /*
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("Autos_PU");
    EntityManager em;
    */
    
   @GET
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
       /** em = emf.createEntityManager();
        List<AutAuto> lista =em.createNamedQuery("AutAuto.findAll").getResultList();
        */
        
        List<AutAuto> lista = dao.findAutAutoEntities();
         
        return Response.ok(200).entity(lista).build();
    }
    
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar){
        /*
        em = emf.createEntityManager();
        AutAuto auto = em.find(AutAuto.class, idbuscar);
        */
        
        AutAuto auto = dao.findAutAuto(idbuscar);
        
        return Response.ok(200).entity(auto).build();
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)     
    public Response nuevo(AutAuto autoNuevo) throws Exception{
    /*
    public String nuevo(AutAuto autoNuevo) throws Exception{
        /*
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(autoNuevo);
        em.getTransaction().commit();
        */
        
        dao.create(autoNuevo);
        
        return Response.ok(200).entity(autoNuevo).build();
        
       // return "Auto Guardado";        
    }
    

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)   
    public Response actualizar(AutAuto autoUpdate) throws Exception{
        /*
        String resultado = null;
      
        em = emf.createEntityManager();
        em.getTransaction().begin();
        autoUpdate = em.merge(autoUpdate);
        em.getTransaction().commit();
        */
        
        dao.edit(autoUpdate);
        
        return Response.ok(200).entity(autoUpdate).build();
        
       //return Response.ok(autoUpdate).build();
    }
    
    
    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    //@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminarId(@PathParam("iddelete") String iddelete) throws NonexistentEntityException{
        /*
        em = emf.createEntityManager();
        em.getTransaction().begin();
        AutAuto autoEliminar = em.getReference(AutAuto.class, iddelete);
        em.remove(autoEliminar);
        em.getTransaction().commit();
        */
        
        AutAuto autodelete = dao.findAutAuto(iddelete);
        
        dao.destroy(iddelete); 
        
        return Response.ok(200).entity(autodelete).build();
        
        //return Response.ok("Auto eliminado").build();
    }
}
