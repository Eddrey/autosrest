

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            table{
                border: 1px solid #000;
                border-collapse: separate;
                /*text-align: center;*/
            }
            table td{
                border: 1px solid #000;                
                padding: 20px;
            }
            table th{
                border: 1px solid #000;
                padding: 10px;
            }
            #titulo{
                margin: 50px 250px;
                width: 900px; 
                text-align: center;
            }
        </style>
        <title>Evaluacion 3 - UNI03</title>
    </head>
    <body>
        <div id = titulo>
        <p><h1>Taller de Aplicaciones Empresariales</h1></p>
        <p><h2>Evaluación Unidad 3-UNI03</h2></p>  
        <p><h3>Alumno: Eddrey Malebranche</h3></p>
        <p><h4>Sección 50</h4></p>
        <p>**********************************************</p>
        </div>
        
        <table>          
            <thead>
                <th>CRUD</th>
                <th>LINKS</th>
                <th>DESCRIPCIONES</th>
            </thead>
            <tbody>
                <tr> 
                    <td>Create-POST</td>
                    <td>/api/autos</td>
                    <td>Crea un nuevo registro en la tabla de la base de datos de Postgres-Heroku a través de un JSON introducido en el body previamente.</td>
                </tr>
                <tr> 
                    <td>Read All-GET</td>
                    <td>/api/autos</td>
                    <td>Lista todos los registros de la tabla de la base de datos Postgres-Heroku en formato JSON.</td>
                </tr>
                <tr> 
                    <td>Read One-GET</td>
                    <td>/api/autos/{id}</td>
                    <td>Lista solo un registro de la tabla de la base de datos Postgres-Heroku en formato JSON. Se debe indicar el ID del registro a listar en el link de consulta.</td>
                </tr>
                <tr> 
                    <td>Update-PUT</td>
                    <td>/api/autos</td>
                    <td>Actualiza un registro en la tabla de la base de datos Postgres-Heroku mediante un JSON introducido en el body. Es importante tener en cuenta el ID del registro a modificar en el JSON puesto que es el único dato que no se actualiza.</td>
                </tr>
                <tr> 
                    <td>Delete-DELETE</td>
                    <td>/api/autos/{id}</td>
                    <td>Elimina un registro en la tabla de la base de datos Postgres-Heroku. Se debe indicar el ID del registro a eliminar en el link de consulta.</td>
                </tr>
            </tbody>
        </table>        
    </body>
</html>
